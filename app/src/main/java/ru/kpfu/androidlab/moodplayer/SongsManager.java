package ru.kpfu.androidlab.moodplayer;

import android.annotation.TargetApi;
import android.media.MediaMetadataRetriever;
import android.os.Build;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import ru.kpfu.androidlab.moodplayer.database.SongsDatabaseHelper;
import ru.kpfu.androidlab.moodplayer.entities.Playlist;
import ru.kpfu.androidlab.moodplayer.entities.Song;


public class SongsManager {
    public static final int MOOD_PLAYLIST = -1;
    public static final int DEFAULT_PLAYLIST = 0;
    private SongsDatabaseHelper helper;

    private Playlist moodPlaylist = new Playlist();
    private Playlist defaultPlaylist;

    public SongsManager(SongsDatabaseHelper helper) {
        this.helper = helper;
    }

    final String MEDIA_PATH = "/mnt/";

    public Playlist getPlayList() {
        Playlist allSongs = helper.getAllSongs();
        if (allSongs == null || allSongs.size() == 0) {
            scan();
            allSongs = helper.getAllSongs();
        }
        return defaultPlaylist = allSongs;
    }
    public Playlist getPlayList(int moodLevel) {
        return moodPlaylist = helper.getMoodPlayList(moodLevel);
    }

    public void scan() {
        File home = new File(MEDIA_PATH);
        Playlist songs = new Playlist();
        try {
            getSongs(songs, home);
        }
        catch (Exception e) {}
        helper.setAllSongs(songs);
    }

    private void getSongs(Playlist songs, File file) {
        File[] tracks = file.listFiles(new FileExtensionFilter());
        if (tracks != null && tracks.length > 0) {
            for (File f : tracks) {
                Song song = new Song();
                song.setFileName(f.getName().substring(0, (f.getName().length() - 4)));
                song.setRoot(f.getPath());
                song.setHash(f.hashCode());
                setupMetadata(song);
                if (song.getArtist() != null && song.getTrackName() != null)
                    song.setTitle(song.getArtist() +" - "+ song.getTrackName());
                else
                    song.setTitle(song.getFileName());
                songs.add(song);
            }
        }
        File[] files = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });

        if (files == null) return;
        for (File f : files) {
            getSongs(songs, f);
        }
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD_MR1)
    public static void setupMetadata(Song song) {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(song.getRoot());

        String albumName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
        String artist = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        String duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        String trackName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);

        song.setAlbum(albumName);
        song.setArtist(artist);
        song.setDuration(Long.parseLong(duration));
        song.setTrackName(trackName);
    }

    public void updateSong(Song currentSong) {
        helper.updateSong(currentSong);
    }

    public List<Playlist> getUserPlaylists() {
        return new ArrayList<>();
    }

    public Playlist getPlayListById(int playlistId) {
        switch (playlistId) {
            case MOOD_PLAYLIST: return moodPlaylist;
            case DEFAULT_PLAYLIST : return defaultPlaylist == null ? getPlayList() : defaultPlaylist;
            default: return helper.getPlayListById(playlistId);
        }
    }

    class FileExtensionFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            return (name.endsWith(".mp3") || name.endsWith(".MP3"));
        }
    }
}
