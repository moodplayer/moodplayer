package ru.kpfu.androidlab.moodplayer;

import ru.kpfu.androidlab.moodplayer.entities.Song;

public interface AudioPlayerListener {

    void onChangeSong(Song newSong);

    void onChangeState(boolean paused);
}
