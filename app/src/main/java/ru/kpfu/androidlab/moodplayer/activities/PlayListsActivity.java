package ru.kpfu.androidlab.moodplayer.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import ru.kpfu.androidlab.moodplayer.MyApp;
import ru.kpfu.androidlab.moodplayer.R;
import ru.kpfu.androidlab.moodplayer.SongListAdapter;
import ru.kpfu.androidlab.moodplayer.SongsManager;
import ru.kpfu.androidlab.moodplayer.entities.Playlist;
import ru.kpfu.androidlab.moodplayer.views.IconTreeItem;
import ru.kpfu.androidlab.moodplayer.views.holders.PlaylistNodeViewHolder;

public class PlayListsActivity extends Activity {

    private MyApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (MyApp) getApplicationContext();

        TreeNode root = TreeNode.root();
        AndroidTreeView treeView = new AndroidTreeView(this, root);

        TreeNode userPlaylists = new TreeNode(new IconTreeItem(R.layout.playlist_node, "мои плейлисты")).setViewHolder(new PlaylistNodeViewHolder(this, true));

        for (Playlist p : app.getSongManager().getUserPlaylists()) {
            userPlaylists.addChild(new PlaylistNode(p, this));
        }
        root.addChildren(userPlaylists);

        TreeNode current = new TreeNode(new IconTreeItem(R.layout.playlist_node, "все")).setViewHolder(new PlaylistNodeViewHolder(this, false));
        current.addChildren(new PlaylistNode(app.getSongManager().getPlayListById(SongsManager.DEFAULT_PLAYLIST), this));
        root.addChildren(current);

        current = new TreeNode(new IconTreeItem(R.layout.playlist_node, "по настроению")).setViewHolder(new PlaylistNodeViewHolder(this, false));
        current.addChildren(new PlaylistNode(app.getSongManager().getPlayListById(SongsManager.MOOD_PLAYLIST), this));
        root.addChildren(current);

        setContentView(treeView.getView());
    }

    private class PlaylistNode extends TreeNode {

        public PlaylistNode(final Playlist value, final Activity act) {
            super(value);
            setViewHolder(new BaseNodeViewHolder(act) {
                @Override
                public View createNodeView(TreeNode treeNode, Object o) {
                    ListView lw = new ListView(context);

                    lw.setAdapter(new SongListAdapter(act, value));
                    lw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            setResult(100,
                                    new Intent(getApplicationContext(),
                                            AndroidBuildingMusicPlayerActivity.class)
                                            .putExtra("songIndex", position)
                                            .putExtra("playlistIndex", position)
                                            .putExtra("playlistId", value.getId()));
                            finish();
                        }
                    });
                    lw.setMinimumHeight(1500);
                    return lw;
                }
            });
        }
    }
}
