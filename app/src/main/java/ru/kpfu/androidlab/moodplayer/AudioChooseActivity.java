package ru.kpfu.androidlab.moodplayer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class AudioChooseActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        SongInfo songInfo = null;

        if (intent != null && intent.getData() != null) {
            songInfo = new SongInfo(this);
            songInfo.setSongRoot(intent.getData().getPath());
            songInfo.setSongId(-1);
            songInfo.setPosition(0);
            songInfo.saveSongInfo(this);
        }

        AudioService service = ((MyApp) getApplicationContext()).getService();

        if (service == null)
            startService(new Intent(this, AudioService.class));
        else {
            service.setSongInfo(songInfo);
            service.notifyTrack();
        }
    }
}
