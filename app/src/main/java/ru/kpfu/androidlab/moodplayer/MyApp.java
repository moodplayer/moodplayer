package ru.kpfu.androidlab.moodplayer;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

import ru.kpfu.androidlab.moodplayer.database.SongsDatabaseHelper;

public class MyApp extends Application {

    private AudioService mService;

    private SongsManager songManager;
    private SongsDatabaseHelper songsDatabaseHelper;
    private ArrayList<Activity> mActivities = new ArrayList<>();
    private ArrayList<Handler> mHandlers = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        songsDatabaseHelper = new SongsDatabaseHelper(this);
        songManager = new SongsManager(songsDatabaseHelper);
    }

    public AudioService getService() {
        return mService;
    }

    public void setService(AudioService service) {
        this.mService = service;
    }

    public SongsDatabaseHelper getSongsDatabaseHelper() {
        return songsDatabaseHelper;
    }

    public SongsManager getSongManager() {
        return songManager;
    }

    public void setMood(int progress) {
        getService().setPlayList(songManager.getPlayList(progress), 0);
    }

    public void stop() {
        mService.saveSongInfo();
        for (Activity a: mActivities) {
            a.finish();
        }
        getService().stopForeground(false);
        getService().stopSelf();
        getService().stopSelf(Service.START_NOT_STICKY);
        mService = null;
        System.exit(0);
    }

    public void addActivity(Activity activity) {
        mActivities.add(activity);
    }

    public void addHandler(Handler mHandler) {
        if (!mHandlers.contains(mHandler))
            mHandlers.add(mHandler);
    }

    public void removeHandler(Handler mHandler) {
        int i = mHandlers.indexOf(mHandler);
        if (i >= 0)
            mHandlers.remove(i);
    }

    public List<Handler> getHandlers() {
        return mHandlers;
    }
}
