package ru.kpfu.androidlab.moodplayer.broadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ru.kpfu.androidlab.moodplayer.MyApp;

public class NextSongBroadcastReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        ((MyApp)context.getApplicationContext()).getService().nextSong(true);
    }
}
