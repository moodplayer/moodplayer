package ru.kpfu.androidlab.moodplayer.views.nodes;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.unnamed.b.atv.model.TreeNode;

import ru.kpfu.androidlab.moodplayer.SongListAdapter;
import ru.kpfu.androidlab.moodplayer.entities.Playlist;

public class PlaylistNode extends TreeNode {

    public PlaylistNode(final Playlist value, final Activity act, final AdapterView.OnItemClickListener listener) {
        super(value);
        setViewHolder(new BaseNodeViewHolder(act) {
            @Override
            public View createNodeView(TreeNode treeNode, Object o) {
                ListView lw = new ListView(context);

                lw.setAdapter(new SongListAdapter(act, value));
                lw.setOnItemClickListener(listener);
                lw.setMinimumHeight(1500);
                return lw;
            }
        });
    }
}