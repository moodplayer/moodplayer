package ru.kpfu.androidlab.moodplayer.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class HeadsetPluggedActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, AndroidBuildingMusicPlayerActivity.class));
    }
}
