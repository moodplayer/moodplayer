package ru.kpfu.androidlab.moodplayer.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import ru.kpfu.androidlab.moodplayer.SongsManager;
import ru.kpfu.androidlab.moodplayer.entities.Playlist;
import ru.kpfu.androidlab.moodplayer.entities.Song;

public class SongsDatabaseHelper extends SQLiteOpenHelper {

    private static final String INT_TYPE = " int ";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ", \n";

    private static final String DATABASE_NAME = "moodPlayer.db";
    private static final int VERSION = 1;
    public static final String NOT_NULL = " NOT NULL ";

    public SongsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(getCreatesCode());
    }

    private String getCreatesCode() {
        return "CREATE TABLE " + SongEntry.TABLE_NAME + " (" +
                SongEntry._ID + " INTEGER PRIMARY KEY autoincrement" + COMMA_SEP +
                SongEntry.COLUMN_NAME_HASH + INT_TYPE + COMMA_SEP +
                SongEntry.COLUMN_NAME_TITLE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                SongEntry.COLUMN_NAME_ROOT + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                SongEntry.COLUMN_NAME_ALBUM + TEXT_TYPE + COMMA_SEP +
                SongEntry.COLUMN_NAME_ARTIST + TEXT_TYPE + COMMA_SEP +
                SongEntry.COLUMN_NAME_DURATION + INT_TYPE + COMMA_SEP +
                SongEntry.COLUMN_NAME_TRACK_NAME + TEXT_TYPE + COMMA_SEP +
                SongEntry.COLUMN_NAME_MOOD + INT_TYPE +
                " )";
    }

    public Playlist getAllSongs() {
        SQLiteDatabase db = getReadableDatabase();

        String[] projection = {
                SongEntry._ID,
                SongEntry.COLUMN_NAME_TITLE,
                SongEntry.COLUMN_NAME_ROOT,
                SongEntry.COLUMN_NAME_MOOD,
                SongEntry.COLUMN_NAME_ARTIST,
                SongEntry.COLUMN_NAME_ALBUM,
                SongEntry.COLUMN_NAME_DURATION,
                SongEntry.COLUMN_NAME_TRACK_NAME,
        };

        Cursor c = db.query(
                SongEntry.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        Playlist songs = cursorToSongList(c);
        c.close();
        return songs;
    }

    public Playlist getMoodPlayList(int mood) {
        SQLiteDatabase db = getReadableDatabase();

        String[] projection = {
                SongEntry._ID,
                SongEntry.COLUMN_NAME_TITLE,
                SongEntry.COLUMN_NAME_ROOT,
                SongEntry.COLUMN_NAME_MOOD,
                SongEntry.COLUMN_NAME_ARTIST,
                SongEntry.COLUMN_NAME_ALBUM,
                SongEntry.COLUMN_NAME_DURATION,
                SongEntry.COLUMN_NAME_TRACK_NAME,
        };

        Cursor c = db.query(
                SongEntry.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                "(((mood & 127) - " + mood + ") * ((mood & 127) - " + mood + ")\n" +
                        "+\n" +
                        "((mood >> 7) - " + mood + ") * ((mood >> 7) - " + mood + ")) ASC"                                 // The sort order
        );

        Playlist songs = cursorToSongList(c);
        c.close();
        songs.setId(SongsManager.MOOD_PLAYLIST);
        return songs;
    }

    private Playlist cursorToSongList(Cursor c) {
        Playlist ret = new Playlist();
        while (c.moveToNext()) {
            Song s = new Song();
            s.setId(c.getInt(0));
            s.setFileName(c.getString(1));
            s.setRoot(c.getString(2));
            s.setMood(c.getInt(3));
            s.setArtist(c.getString(4));
            s.setAlbum(c.getString(5));
            s.setDuration(c.getInt(6));
            s.setTrackName(c.getString(7));
            if (s.getArtist() != null && s.getTrackName() != null)
                s.setTitle(s.getArtist() + " - " + s.getTrackName());
            else
                s.setTitle(s.getFileName());
            ret.add(s);
        }
        return ret;
    }

    public void setAllSongs(Playlist songs) {
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("delete from " + SongEntry.TABLE_NAME + " ;");
        for (Song s : songs) {
            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(SongEntry.COLUMN_NAME_TITLE, s.getFileName());
            values.put(SongEntry.COLUMN_NAME_ROOT, s.getRoot());
            values.put(SongEntry.COLUMN_NAME_MOOD, s.getMood());
            values.put(SongEntry.COLUMN_NAME_ARTIST, s.getArtist());
            values.put(SongEntry.COLUMN_NAME_DURATION, s.getDuration());
            values.put(SongEntry.COLUMN_NAME_TRACK_NAME, s.getTrackName());

            // Insert the new row, returning the primary key value of the new row
            s.setId((int) db.insert(
                    SongEntry.TABLE_NAME,
                    null,
                    values));
        }
    }

    //TODO: on database update all data will be lost
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + SongEntry.TABLE_NAME);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void updateSong(Song s) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SongEntry.COLUMN_NAME_TITLE, s.getTitle());
        values.put(SongEntry.COLUMN_NAME_ROOT, s.getRoot());
        values.put(SongEntry.COLUMN_NAME_MOOD, s.getMood());
        values.put(SongEntry.COLUMN_NAME_ARTIST, s.getArtist());
        values.put(SongEntry.COLUMN_NAME_DURATION, s.getDuration());
        values.put(SongEntry.COLUMN_NAME_TRACK_NAME, s.getTitle());

        String[] whereArgs = new String[]{String.valueOf(s.getId())};
        db.update(SongEntry.TABLE_NAME,
                values,
                SongEntry._ID + " LIKE ?",
                whereArgs
        );
    }

    public Playlist getPlayListById(int playlistId) {
        return new Playlist();
    }

    public static abstract class SongEntry implements BaseColumns {
        public static final String TABLE_NAME = "songs";

        public static final String COLUMN_NAME_MOOD = "mood";
        public static final String COLUMN_NAME_HASH = "hash";
        public static final String COLUMN_NAME_DURATION = "duration";
        public static final String COLUMN_NAME_TITLE = "fileName";
        public static final String COLUMN_NAME_ROOT = "root";

        public static final String COLUMN_NAME_ALBUM = "album";
        public static final String COLUMN_NAME_ARTIST = "artist";
        public static final String COLUMN_NAME_TRACK_NAME = "trackName";
    }
}
