package ru.kpfu.androidlab.moodplayer;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;

/**
 * Class to operate with state of current
 */
public class SongInfo {

    private static final String SONG_INFO = "songInfo";

    private static final String SONG_ID = "songId";
    private static final String POSITION = "playerCurrentPosition";
    private static final String SONG_ROOT = "songRoot";

    private int songId;
    private String songRoot;
    private int position;

    public int getSongId() {
        return songId;
    }

    public void setSongId(int songId) {
        this.songId = songId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public SongInfo() {
    }

    public SongInfo(ContextWrapper contextWrapper) {
        SharedPreferences preferences = contextWrapper.getSharedPreferences(SONG_INFO, Context.MODE_PRIVATE);
        setSongId(preferences.getInt(SONG_ID, -1));
        setSongRoot(preferences.getString(SONG_ROOT, null));
        setPosition(preferences.getInt(POSITION, -1));
    }

    public void saveSongInfo(ContextWrapper contextWrapper) {
        SharedPreferences preferences = contextWrapper.getSharedPreferences(SONG_INFO, Context.MODE_PRIVATE);
        preferences.edit()
                .putInt(SONG_ID, songId)
                .putInt(POSITION, position)
                .putString(SONG_ROOT, songRoot)
                .commit();
    }

    public String getSongRoot() {
        return songRoot;
    }

    public void setSongRoot(String songRoot) {
        this.songRoot = songRoot;
    }
}
