package ru.kpfu.androidlab.moodplayer.entities;

import java.util.ArrayList;

import ru.kpfu.androidlab.moodplayer.SongsManager;

public class Playlist extends ArrayList<Song> {

    private String name;

    /**
     * Id of playlist.
     * -1 is mood playlist
     * 0 is default
     */
    private int id;

    public Playlist() {
        super();
        this.name = "default";
        this.id = SongsManager.DEFAULT_PLAYLIST;
    }

    public Playlist(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return name;
    }
}
