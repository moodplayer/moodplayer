package ru.kpfu.androidlab.moodplayer.views;

public class IconTreeItem {
    public int layoutId;
    public String text;

    public IconTreeItem(int layoutId, String text) {
        this.layoutId = layoutId;
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}