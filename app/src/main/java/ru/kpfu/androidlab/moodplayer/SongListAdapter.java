package ru.kpfu.androidlab.moodplayer;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import ru.kpfu.androidlab.moodplayer.entities.Playlist;
import ru.kpfu.androidlab.moodplayer.entities.Song;

import static ru.kpfu.androidlab.moodplayer.Utilities.milliSecondsToTimer;

public class SongListAdapter extends ArrayAdapter<Song> {

    private final Activity context;
    private final Playlist songs;
    private static ViewHolder vh;

    public SongListAdapter(Activity context, Playlist songs) {
        super(context, R.layout.playlist_item, songs);
        this.context = context;
        this.songs = songs;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.playlist_item, null, false);

            vh = new ViewHolder();
            vh.songTitle = (TextView) convertView.findViewById(R.id.songTitle);
            vh.songDuration = (TextView) convertView.findViewById(R.id.songDuration);

            convertView.setTag(vh);
        }

        vh = (ViewHolder) convertView.getTag();
        Song song = songs.get(position);

        vh.songTitle.setText(song.getTitle());
        vh.songDuration.setText(milliSecondsToTimer(song.getDuration()));
        return convertView;
    }

    private static class ViewHolder {
        private TextView songTitle;
        private TextView songDuration;
    }
}
