package ru.kpfu.androidlab.moodplayer.entities;

import java.io.Serializable;

public class Song implements Serializable{

    private int id;
    private int mood;
    private int hash;
    private long duration;
    private String fileName;
    private String root;

    private String album;
    private String artist;
    private String trackName;
    private String title;

    public Song() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public void setMood(int start, int end) {
        int result = start << 7;
        result += end;
        this.mood = result;
    }

    public void setMood(int mood) {
        this.mood = mood;
    }

    public int getMood() {
        return mood;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    public int getHash() {
        return hash;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist == null ? "" : artist;
    }

    public String getAlbum() {
        return album == null ? "" : album;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getTitle() {
        return title == null ? "" : title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMoodStart() {
        return mood ==0 ? 0 : mood >> 7;
    }

    public int getMoodlEnd() {
        return mood == 0 ? 127 : mood & 127;
    }
}
