package ru.kpfu.androidlab.moodplayer;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;

import java.io.IOException;
import java.util.Random;

import ru.kpfu.androidlab.moodplayer.entities.Playlist;
import ru.kpfu.androidlab.moodplayer.entities.Song;

public class AudioPlayer extends MediaPlayer implements MediaPlayer.OnCompletionListener {

    private AudioPlayerListener listener;

    private static final String T = AudioPlayer.class.getName();
    private Playlist playList;

    private int currentIndex;
    private Song currentSong;

    private boolean repeatMode = false;
    private boolean shuffleMode = false;

    private boolean play = false;
    private SongInfo songInfo;

    public AudioPlayer(AudioPlayerListener listener) {
        super();
        this.listener = listener;
        setOnCompletionListener(this);
        setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    /**
     * Changes state of AudioPlayer
     */
    public boolean changeState() {

        if (isPlaying()) {
            play = false;
            pause();
            listener.onChangeState(true);
            return false;
        } else {
            play = true;
            start();
            listener.onChangeState(false);
            return true;
        }

    }

    public void setPlayList(Playlist songs) {
        currentIndex = 0;
        playList = songs;
        if (play) {
            playDefault();
        }
    }

    public void setPlayList(Playlist songs, int currentIndex) {
        this.currentIndex = currentIndex;
        playList = songs;
        playSong(currentIndex);
    }

    public void playDefault() {
        if (songInfo.getSongId() != -1)
            for (int i = 0; i < playList.size(); i++) {
                Song s = get(i);
                if (s.getId() == songInfo.getSongId()) {
                    currentIndex = i;
                    playSong(get(i), songInfo.getPosition());
                    break;
                }
            }
        else if (songInfo.getSongRoot() != null) {
            Song s = new Song();
            s.setRoot(songInfo.getSongRoot());
            SongsManager.setupMetadata(s);
            playSong(s);
        }
        else playSong(get(0), -1);
    }

    public void playSong(int songIndex) {
        playSong(get(songIndex), -1);
    }

    public void playSong(Song song) {
        playSong(song, -1);
    }

    public void playSong(Song song, int position) {
        try {
            listener.onChangeSong(song);
            reset();
            setAudioStreamType(AudioManager.STREAM_MUSIC);
            setDataSource(song.getRoot());
            currentSong = song;
            prepare();
            if (position != -1) {
                seekTo(position);
            }
            start();
        } catch (IOException e) {
            Log.e(T, "playSong error:\n", e);
        }
    }

    public void next(boolean forward) {
        int size = playList.size();
        playSong(get(forward ? ++currentIndex % size : --currentIndex % size));
    }


    private Song get(int location) {
        if (playList.size() <= location)
            return new Song();
        return playList.get(location);
    }

    public void setSongInfo(SongInfo songInfo) {
        this.songInfo = songInfo;
    }

    public Song getCurrentSong() {
        return currentSong;
    }

    /**
     * On Song Playing completed
     * if repeat is ON play same song again
     * if shuffle is ON play random song
     */
    @Override
    public void onCompletion(MediaPlayer arg0) {

        // check for repeat is ON or OFF
        if (repeatMode) {
            // repeat is on play same song again
            playSong(currentSong);
        } else if (shuffleMode) {
            // shuffle is on - play a random song
            Random rand = new Random();
            currentIndex = rand.nextInt((playList.size() - 1) + 1);
            playSong(currentIndex);
        } else {
            // no repeat or shuffle ON - play next song
            if (currentIndex < (playList.size() - 1)) {
                playSong(currentIndex + 1);
                currentIndex = currentIndex + 1;
            } else {
                // play first song
                playSong(0);
                currentIndex = 0;
            }
        }
    }

    public boolean toggleRepeat() {
        return repeatMode = !repeatMode;
    }

    public boolean toggleShuffle() {
        return shuffleMode= !shuffleMode;
    }
}
