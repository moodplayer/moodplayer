package ru.kpfu.androidlab.moodplayer.views;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;

import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import ru.kpfu.androidlab.moodplayer.MyApp;
import ru.kpfu.androidlab.moodplayer.R;
import ru.kpfu.androidlab.moodplayer.SongsManager;
import ru.kpfu.androidlab.moodplayer.entities.Playlist;
import ru.kpfu.androidlab.moodplayer.views.holders.PlaylistNodeViewHolder;
import ru.kpfu.androidlab.moodplayer.views.nodes.PlaylistNode;

public class PlaylistsView {

    public View getView(Activity context, AdapterView.OnItemClickListener listener1, AdapterView.OnItemClickListener listener2, AdapterView.OnItemClickListener listener3) {
        MyApp app = (MyApp) context.getApplicationContext();

        TreeNode root = TreeNode.root();
        AndroidTreeView treeView = new AndroidTreeView(context, root);

        TreeNode userPlaylists = new TreeNode(new IconTreeItem(R.layout.playlist_node, "мои плейлисты")).setViewHolder(new PlaylistNodeViewHolder(context, true));
        for (Playlist p : app.getSongManager().getUserPlaylists()) {
            userPlaylists.addChild(new PlaylistNode(p, context, listener1));
        }
        root.addChildren(userPlaylists);

        TreeNode current = new TreeNode(new IconTreeItem(R.layout.playlist_node, "все")).setViewHolder(new PlaylistNodeViewHolder(context, false));
        current.addChildren(new PlaylistNode(app.getSongManager().getPlayListById(SongsManager.DEFAULT_PLAYLIST), context, listener2));
        root.addChildren(current);

        current = new TreeNode(new IconTreeItem(R.layout.playlist_node, "по настроению")).setViewHolder(new PlaylistNodeViewHolder(context, false));
        current.addChildren(new PlaylistNode(app.getSongManager().getPlayListById(SongsManager.MOOD_PLAYLIST), context, listener3));
        root.addChildren(current);

        return treeView.getView();
    }
}
