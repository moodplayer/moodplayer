package ru.kpfu.androidlab.moodplayer.views.holders;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.unnamed.b.atv.model.TreeNode;

import ru.kpfu.androidlab.moodplayer.R;
import ru.kpfu.androidlab.moodplayer.views.IconTreeItem;

public class PlaylistNodeViewHolder extends TreeNode.BaseNodeViewHolder<IconTreeItem> implements View.OnClickListener {

    private boolean showAddButton;

    public PlaylistNodeViewHolder(Context context, boolean b) {
        super(context);
        showAddButton = b;
    }

    @Override
    public View createNodeView(TreeNode treeNode, IconTreeItem iconTreeItem) {
        Activity a = (Activity) context;
        LayoutInflater inflater = a.getLayoutInflater();
        View view = inflater.inflate(iconTreeItem.layoutId, null, false);
        TextView t = (TextView) view.findViewById(R.id.playlist_node_title);
        t.setText(iconTreeItem.text);
        ImageButton addButton = (ImageButton) view.findViewById(R.id.playlist_add);
        if (showAddButton) {
            addButton.setVisibility(View.VISIBLE);
            addButton.setOnClickListener(this);
        }
        return view;
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(context, "Unrealized yet", Toast.LENGTH_LONG).show();
//        View view = LayoutInflater.from(context).inflate(R.layout.playlist_edit_window, null);
//
//        AlertDialog.Builder b = new AlertDialog.Builder(context)
//                .setView(view)
    }
}
