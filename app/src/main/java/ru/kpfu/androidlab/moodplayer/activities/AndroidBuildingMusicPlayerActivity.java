package ru.kpfu.androidlab.moodplayer.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.kpfu.androidlab.moodplayer.AudioService;
import ru.kpfu.androidlab.moodplayer.MyActivity;
import ru.kpfu.androidlab.moodplayer.MyApp;
import ru.kpfu.androidlab.moodplayer.R;
import ru.kpfu.androidlab.moodplayer.SongInfo;
import ru.kpfu.androidlab.moodplayer.Utilities;
import ru.kpfu.androidlab.moodplayer.entities.Song;
import ru.kpfu.androidlab.moodplayer.views.RangeSeekBar;

public class AndroidBuildingMusicPlayerActivity extends MyActivity implements SeekBar.OnSeekBarChangeListener {

    private ImageButton btnPlay;
    private ImageButton btnRepeat;
    private ImageButton btnShuffle;
    private SeekBar songProgressBar;
    private TextView songTitleLabel;
    private TextView songCurrentDurationLabel;
    private TextView songTotalDurationLabel;

    // Handler to update UI timer, progress bar etc,.
    private static Handler mHandler;

    private RangeSeekBar<Integer> moodBar;
    private SeekBar moodLevel;

    private MyApp app;

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        app.addHandler(mHandler);
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        app.removeHandler(mHandler);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkIntent();

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Song song = (Song) msg.obj;
                songTitleLabel.setText(song.getTitle());
                getSupportActionBar().setTitle(song.getTitle());
                moodBar.setValues(song.getMoodStart(), song.getMoodlEnd());
                btnPlay.setImageResource(R.drawable.btn_pause);
                updateProgressBar();
            }
        };

        app = (MyApp) getApplicationContext();
        app.addActivity(this);
        app.addHandler(mHandler);

        setContentView(R.layout.player);

        startAudioService();

        // All player buttons
        btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        ImageButton btnNext = (ImageButton) findViewById(R.id.btnNext);
        ImageButton btnPrevious = (ImageButton) findViewById(R.id.btnPrevious);
        btnRepeat = (ImageButton) findViewById(R.id.btnRepeat);
        btnShuffle = (ImageButton) findViewById(R.id.btnShuffle);
        songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
        songTitleLabel = (TextView) findViewById(R.id.songTitle);
        songCurrentDurationLabel = (TextView) findViewById(R.id.songCurrentDurationLabel);
        songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);
        ImageButton trackEdit = (ImageButton) findViewById(R.id.track_edit_button);
        trackEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Song currentSong = app.getService().getCurrentSong();
                AlertDialog.Builder builder = new AlertDialog.Builder(AndroidBuildingMusicPlayerActivity.this);
                final List<String> strings = createSongNameVariants(currentSong);
                final View trackRenameWindow = getLayoutInflater().inflate(R.layout.track_rename_window, null);
                final EditText trackName = (EditText) trackRenameWindow.findViewById(R.id.track_name);
                trackName.setText(currentSong.getTitle());
                ListView trackNameVariants = (ListView) trackRenameWindow.findViewById(R.id.track_name_variants);

                trackNameVariants.setAdapter(new ArrayAdapter<>(AndroidBuildingMusicPlayerActivity.this, android.R.layout.simple_list_item_1, strings));
                trackNameVariants.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        trackName.setText(strings.get(position));
                    }
                });
                builder.setView(trackRenameWindow)
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String newName = trackName.getText().toString();
                                currentSong.setTitle(newName);
                                app.getSongManager().updateSong(currentSong);
                                app.getService().onChangeSong(currentSong);
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });

        //Creating track mood bar
        createMoodBar();

        //Creating mood bar
        createMoodLevelBar();

        // Listeners
        songProgressBar.setOnSeekBarChangeListener(this); // Important

        btnPlay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (app.getService().changeState()) {
                    btnPlay.setImageResource(R.drawable.btn_pause);
                } else {
                    btnPlay.setImageResource(R.drawable.btn_play);
                }
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                app.getService().nextSong(true);

            }
        });

        btnPrevious.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                app.getService().nextSong(true);
            }
        });

        btnRepeat.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (app.getService().toggleRepeat())
                    btnRepeat.setImageResource(R.drawable.n_img_btn_repeat_pressed);
                else
                    btnRepeat.setImageResource(R.drawable.btn_repeat);
            }
        });

        btnShuffle.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (app.getService().toggleShuffle())
                    btnShuffle.setImageResource(R.drawable.n_img_btn_shuffle_pressed);
                else
                    btnShuffle.setImageResource(R.drawable.btn_shuffle);
            }
        });
    }

    private List<String> createSongNameVariants(Song song) {
        // Ensure method does not return null!
        List<String> variants = new ArrayList<>();
        variants.add(song.getTitle());
        variants.add(song.getArtist() + " - " + song.getTrackName());
        variants.add(song.getFileName());
        variants.add(song.getAlbum());

        while (variants.remove("") || variants.remove(" - ")) {
        }
        return variants;
    }

    private void createMoodLevelBar() {
        moodLevel = new SeekBar(this);
        moodLevel.setMax(127);
        moodLevel.setProgress(getMood());
        moodLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                app.setMood(moodLevel.getProgress());
            }
        });
        ((TableLayout) findViewById(R.id.songThumbnail))
                .addView(moodLevel, 1);
    }

    private void startAudioService() {
        if (app.getService() == null)
            startService(new Intent(this, AudioService.class));
        else
            app.getService().notifyTrack();
    }

    /**
     * Check if activity started with open mp3 intent
     */
    private void checkIntent() {
        Intent intent = getIntent();
        if (intent != null && intent.getData() != null) {
            SongInfo songInfo = new SongInfo(this);
            songInfo.setSongRoot(intent.getData().getPath());
            songInfo.setSongId(-1);
            songInfo.setPosition(0);
            songInfo.saveSongInfo(this);
        }
    }

    private int getMood() {
        SharedPreferences mood = getSharedPreferences("mood", MODE_PRIVATE);
        return mood.getInt("mood", 50);
    }

    private void createMoodBar() {
        moodBar = new RangeSeekBar<>(0, 127, this);
        moodBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                Song currentSong = app.getService().getCurrentSong();
                currentSong.setMood(minValue, maxValue);
                app.getSongManager().updateSong(currentSong);
            }
        });

        ((TableLayout) findViewById(R.id.songThumbnail))
                .addView(moodBar, 0);
    }

    private void onUpdatePlayList() {
        app.getSongManager().scan();
    }

    private void onOpenPlayList() {
        Intent i = new Intent(getApplicationContext(), PlayListsActivity.class);
        startActivityForResult(i, 100);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_playlist:
                onOpenPlayList();
                return true;
            case R.id.action_update:
                onUpdatePlayList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Receiving song index from playlist view
     * and play the song
     */
    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            Bundle extras = data.getExtras();
            int playlistId = extras.getInt("playlistId");
            int playlistIndex = extras.getInt("playlistIndex");
            int songIndex = extras.getInt("songIndex");

            app.getService().setPlayList(app.getSongManager().getPlayListById(playlistId), playlistIndex);
            app.getService().playSong(songIndex);
        }
    }

    /**
     * Update timer on seekbar
     */
    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    /**
     * Background Runnable thread
     */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            try {
                long totalDuration = app.getService().getCurrentSong().getDuration();
                long currentDuration = app.getService().getCurrentPosition();

                // Displaying Total Duration time
                songTotalDurationLabel.setText("" + Utilities.milliSecondsToTimer(totalDuration));
                // Displaying time completed playing
                songCurrentDurationLabel.setText("" + Utilities.milliSecondsToTimer(currentDuration));

                // Updating progress bar
                int progress = Utilities.getProgressPercentage(currentDuration, totalDuration);
                //Log.d("Progress", ""+progress);
                songProgressBar.setProgress(progress);

                // Running this thread after 100 milliseconds
                mHandler.postDelayed(this, 100);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     *
     * */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {

    }

    /**
     * When user starts moving the progress handler
     */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    /**
     * When user stops moving the progress hanlder
     */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        app.getService().seekTo(seekBar.getProgress());
        updateProgressBar();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        saveInfo();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        saveInfo();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        saveInfo();
        super.onBackPressed();
    }

    private void saveInfo() {
        SharedPreferences mood = getSharedPreferences("mood", MODE_PRIVATE);
        mood.edit()
                .putInt("mood", moodLevel.getProgress())
                .commit();
        AudioService service = app.getService();
        if (service != null) {
            service.saveSongInfo();
        }
    }
}