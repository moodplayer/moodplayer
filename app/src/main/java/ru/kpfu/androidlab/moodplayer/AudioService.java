package ru.kpfu.androidlab.moodplayer;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.widget.RemoteViews;

import ru.kpfu.androidlab.moodplayer.broadcastReceivers.NextSongBroadcastReceiver;
import ru.kpfu.androidlab.moodplayer.broadcastReceivers.PlayPauseBroadcastReceiver;
import ru.kpfu.androidlab.moodplayer.broadcastReceivers.StopPlayBroadcastReceiver;
import ru.kpfu.androidlab.moodplayer.entities.Playlist;
import ru.kpfu.androidlab.moodplayer.entities.Song;

public class AudioService extends Service implements AudioPlayerListener {

    private RemoteViews notificationView;
    private Notification n;

    private AudioPlayer mp;

    public static final String PLAY_PAUSE_ACTION = "ru.kpfu.androidlab.moodplayer.PLAY_PAUSE_ACTION";
    private MyApp app;

    @Override
    public IBinder onBind(Intent intent) {
        app = (MyApp) getApplicationContext();
        init();
        return new Binder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int r = super.onStartCommand(intent, flags, startId);
        app = (MyApp) getApplicationContext();
        init();
        return r;
    }

    private void init() {
        mp = new AudioPlayer(this);
        app.setService(this);

        mp.setSongInfo(new SongInfo(this));
        Playlist allSongs = app.getSongsDatabaseHelper().getAllSongs();
        if(allSongs.size() == 0) {
            app.getSongManager().scan();
            allSongs = app.getSongsDatabaseHelper().getAllSongs();
        }
        mp.setPlayList(allSongs);

        n = setNotification("Mood Player", true);

        setSongInfo(new SongInfo(this));
        mp.playDefault();
    }

    private Notification setNotification(String text, boolean show) {
        n = new Notification(R.drawable.ic_launcher, text, System.currentTimeMillis());
        notificationView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.notification);

        Intent i = new Intent(getApplicationContext(), PlayPauseBroadcastReceiver.class);
        notificationView.setOnClickPendingIntent(R.id.notif_change_state, PendingIntent.getBroadcast(getApplicationContext(), 1, i, 0));

        i = new Intent(getApplicationContext(), NextSongBroadcastReceiver.class);
        notificationView.setOnClickPendingIntent(R.id.notif_next, PendingIntent.getBroadcast(getApplicationContext(), 1, i, 0));

        i = new Intent(getApplicationContext(), StopPlayBroadcastReceiver.class);
        notificationView.setOnClickPendingIntent(R.id.notif_close, PendingIntent.getBroadcast(getApplicationContext(), 1, i, 0));


        n.contentView = notificationView;
        if (show)
            startForeground(1, n);
        return n;
    }

    /**
     * Changes state of playing service
     *
     * @return true if changed to play mode, else otherwise
     */
    public boolean changeState() {
        try {
            return mp.changeState();
        }
        catch (Exception e) {
            init();
            return mp.changeState();
        }
    }

    /**
     * set playlist and play default (usually, from SongInfo; first otherwise)
     * @param songs - playlists to play
     */
    public void setPlayList(Playlist songs) {
        mp.setPlayList(songs);
    }

    /**
     * Sets playlist and plays current index
     * @param songs - playlist to play
     * @param current - index of song to play
     */
    public void setPlayList(Playlist songs, int current) {
        mp.setPlayList(songs, current);
    }

    public void nextSong(boolean forward) {
        mp.next(forward);
    }

    public void playSong(int i) {
        mp.playSong(i);
    }

    public void setSongInfo(SongInfo songInfo) {
        mp.setSongInfo(songInfo);
    }

    public int getCurrentPosition() {
        return mp.getCurrentPosition();
    }

    public Song getCurrentSong() {
        return mp.getCurrentSong();
    }

    public boolean toggleRepeat() {
       return mp.toggleRepeat();
    }

    public boolean toggleShuffle() {
        return mp.toggleShuffle();
    }

    public void saveSongInfo() {
        SongInfo songInfo = new SongInfo();
        songInfo.setSongId(getCurrentSong().getId());
        songInfo.setPosition(getCurrentPosition());
        songInfo.saveSongInfo(this);
    }

    @Override
    public void onDestroy() {
        mp.stop();
        super.onDestroy();
        saveSongInfo();
        mp.release();
        stopForeground(false);
    }

    @Override
    public void onLowMemory() {
        saveSongInfo();
    }

    public void seekTo(int progress) {
        int totalDuration = mp.getDuration();
        int currentPosition = Utilities.progressToTimer(progress, totalDuration);
        mp.seekTo(currentPosition);
    }

    public void notifyTrack() {
        mp.setSongInfo(new SongInfo(this));
        mp.playDefault();
    }

    @Override
    public void onChangeSong(Song newSong) {
        Message m = new Message();
        m.obj = newSong;
        for (Handler h : app.getHandlers()) {
            h.sendMessage(m);
        }
        n = setNotification(newSong.getTitle(), false);
        notificationView.setCharSequence(R.id.notif_song_title, "setText", newSong.getTitle());
        startForeground(1, n);
    }

    @Override
    public void onChangeState(boolean paused) {
        notificationView.setInt(R.id.notif_change_state, "setImageResource", paused ? R.drawable.btn_pause : R.drawable.not_play);
        startForeground(1, n);
    }
}
